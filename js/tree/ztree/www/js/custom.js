/* eslint-disable no-undef */

var setting = {
  view: {
    addHoverDom: addHoverDom, // Hover出現
    removeHoverDom: removeHoverDom, // Hover消失
		selectedMulti: false,
		
  },
  edit: {
    enable: true,
		editNameSelectAll: false,
		showRenameBtn: false, //不可rename
		removeTitle:"刪除",
		showRemoveBtn: showRemoveBtn,   //在非level 0層都顯示 刪除
		showCustomBtns: showCustomBtns, //在非level 0層都顯示 檢視/編輯
  },
  data: {
    simpleData: {
      enable: true,
    },
  },
  callback: {
    beforeDrag: beforeDrag,
    beforeRemove: beforeRemove,
		onRemove: onRemove
  },
};

var zNodes = [
  { id: 1, pid: 0, name: "LV1_服裝與飾品", open: true },
  { id: 11, pid: 1, name: "LV2_衣服", url: "http://www.google.com" },
  { id: 12, pid: 1, name: "LV2_鞋子", url: "http://www.google.com" },
  { id: 13, pid: 1, name: "LV2_褲子", url: "http://www.google.com" },
  
  { id: 2, pid: 0, name: "LV1_嬰兒與兒童", open: true },
  { id: 21, pid: 2, name: "LV2_奶嘴" },
  { id: 22, pid: 2, name: "LV2_卡丁車" },
  { id: 23, pid: 2, name: "LV2_拉拉拉" },
];

var log,
  className = "dark";

function beforeDrag(treeId, treeNodes) {
  return false;
}

function beforeRemove(treeId, treeNode) {
  className = className === "dark" ? "" : "dark";
  showLog(
    "[ " +
      getTime() +
      " 刪除 ]&nbsp;&nbsp;&nbsp;&nbsp; " +
      treeNode.name
  );
  var zTree = $.fn.zTree.getZTreeObj("treeDemo");
  zTree.selectNode(treeNode);
  return confirm("確定刪除" + treeNode.name + " 嗎?");
}

function onRemove(e, treeId, treeNode) {
  showLog(
    "[ " + getTime() + " 刪除 ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name
  );
}

//刪除
function showRemoveBtn(treeId, treeNode) {
  return treeNode.level !== 0;
}

function showLog(str) {
  if (!log) log = $("#log");
  log.append("<li class='" + className + "'>" + str + "</li>");
  if (log.children("li").length > 8) {
    log.get(0).removeChild(log.children("li")[0]);
  }
}

function getTime() {
  var now = new Date(),
    h = now.getHours(),
    m = now.getMinutes(),
    s = now.getSeconds(),
    ms = now.getMilliseconds();
  return h + ":" + m + ":" + s + " " + ms;
}

var newCount = 1;
function addHoverDom(treeId, treeNode) {
  var selectObj = $("#" + treeNode.tId + "_span"); //選擇的物件

	if (
			treeNode.editNameFlag || 
			$("#" + treeNode.tId + "_viewBtn").length > 0 ||
			$("#" + treeNode.tId + "_edtiBtn").length > 0
		)
	return;

	//檢視,編輯
  var customBtns =
		"<span class='button view custombtns' id=" + treeNode.tId + "_viewBtn title='檢視' onfocus='this.blur();'></span>" + 
		"<span class='button edit custombtns' id=" + treeNode.tId + "_editBtn title='編輯' onfocus='this.blur();'></span>";
  selectObj.after(customBtns);

  var viewBtn = $("#" + treeNode.tId + "_viewBtn");
  var editBtn = $("#" + treeNode.tId + "_editBtn");

	//click 檢視
  if (viewBtn)
    viewBtn.bind("click", function () {

			showLog(
				"[ " +
					getTime() +
					" 檢視 ]&nbsp;&nbsp;&nbsp;&nbsp; " +
					treeNode.name + "url:" + treeNode.url
			);

    });

  // click 編輯
  if (editBtn)
    editBtn.bind("click", function () {
			alert("開新layer 編輯");
			
			showLog(
				"[ " +
					getTime() +
					" 編輯 ]&nbsp;&nbsp;&nbsp;&nbsp; " +
					treeNode.name
			);
    });
}

function showCustomBtns(treeId, treeNode) {
  return treeNode.level !== 0;
}

function removeHoverDom(treeId, treeNode) {
	$("#" + treeNode.tId + "_viewBtn").unbind().remove();
	$("#" + treeNode.tId + "_editBtn").unbind().remove();
}

function selectAll() {
  var zTree = $.fn.zTree.getZTreeObj("treeDemo");
  zTree.setting.edit.editNameSelectAll = $("#selectAll").attr("checked");
}

$(document).ready(function () {
  $.fn.zTree.init($("#treeDemo"), setting, zNodes);
  $("#selectAll").bind("click", selectAll);
});