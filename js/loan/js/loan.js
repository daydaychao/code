  //id 
  out1 = document.getElementById('total-monthy');
  out2 = document.getElementById('total-interest');
  out3 = document.getElementById('total-payment');

//等额本息计算
function benxi (type, num, year, lilv) {
    //每月月供额=〔贷款本金×月利率×(1＋月利率)＾还款月数〕÷〔(1＋月利率)＾还款月数-1〕
    var mouth = parseInt(year) * 12,
        mouthlilv = parseFloat(lilv) / 12,
        dknum = parseFloat(num) * 10000;
    //每月月供
    var yuegong = (dknum * mouthlilv * Math.pow((1 + mouthlilv), mouth)) / (Math.pow((1 + mouthlilv), mouth) - 1);
    //总利息=还款月数×每月月供额-贷款本金
    var totalLixi = mouth * yuegong - dknum;
    //还款总额 总利息+贷款本金
    var totalPrice = totalLixi + dknum,
        leftFund = totalLixi + dknum;

    //循环月份
    var mouthdataArray = [],
        nowmouth = new Date().getMonth(),
        realmonth = 0;

    for (var i = 1; i <= mouth; i++) {
        realmonth = nowmouth + i;
        var yearlist = Math.floor(i / 12);

        realmonth = realmonth - 12 * yearlist;

        if (realmonth > 12) {
            realmonth = realmonth - 12
        }
        //console.log(realmonth)
        //每月应还利息=贷款本金×月利率×〔(1+月利率)^还款月数-(1+月利率)^(还款月序号-1)〕÷〔(1+月利率)^还款月数-1〕
        var yuelixi = dknum * mouthlilv * (Math.pow((1 + mouthlilv), mouth) - Math.pow((1 + mouthlilv), i - 1)) / (Math.pow((1 + mouthlilv), mouth) - 1);
        //每月应还本金=贷款本金×月利率×(1+月利率)^(还款月序号-1)÷〔(1+月利率)^还款月数-1〕
        var yuebenjin = dknum * mouthlilv * Math.pow((1 + mouthlilv), i - 1) / (Math.pow((1 + mouthlilv), mouth) - 1);
        leftFund = leftFund - (yuelixi + yuebenjin);
        if (leftFund < 0) {
            leftFund = 0
        }
        mouthdataArray[i - 1] = {
            monthName: realmonth + "月",
            yuelixi: yuelixi,
            yuebenjin: yuebenjin,
            //剩余还款
            leftFund: leftFund
        }
    }
    return {
        yuegong: yuegong,
        totalLixi: totalLixi,
        totalPrice: totalPrice,
        mouthdataArray: mouthdataArray,
        totalDknum: num,
        year: year
    };
}

//等额本金计算
function benjin (type, num, year, lilv) {
    var mouth = parseInt(year) * 12,
        mouthlilv = parseFloat(lilv) / 12,
        dknum = parseFloat(num) * 10000,
        yhbenjin = 0; //首月还款已还本金金额是0
    //每月应还本金=贷款本金÷还款月数
    var everymonthyh = dknum / mouth
    //每月月供额=(贷款本金÷还款月数)+(贷款本金-已归还本金累计额)×月利率
    var yuegong = everymonthyh + (dknum - yhbenjin) * mouthlilv;
    //每月月供递减额=每月应还本金×月利率=贷款本金÷还款月数×月利率
    var yuegongdijian = everymonthyh * mouthlilv;
    //总利息=〔(总贷款额÷还款月数+总贷款额×月利率)+总贷款额÷还款月数×(1+月利率)〕÷2×还款月数-总贷款额
    var totalLixi = ((everymonthyh + dknum * mouthlilv) + dknum / mouth * (1 + mouthlilv)) / 2 * mouth - dknum;
    //还款总额 总利息+贷款本金
    var totalPrice = totalLixi + dknum,
        leftFund = totalLixi + dknum;

    //循环月份
    var mouthdataArray = [],
        nowmouth = new Date().getMonth(),
        realmonth = 0;

    for (var i = 1; i <= mouth; i++) {
        realmonth = nowmouth + i;
        var yearlist = Math.floor(i / 12);

        realmonth = realmonth - 12 * yearlist;

        if (realmonth > 12) {
            realmonth = realmonth - 12
        }
        yhbenjin = everymonthyh * (i - 1);
        var yuebenjin = everymonthyh + (dknum - yhbenjin) * mouthlilv;
        //每月应还利息=剩余本金×月利率=(贷款本金-已归还本金累计额)×月利率
        var yuelixi = (dknum - yhbenjin) * mouthlilv;
        leftFund = leftFund - yuebenjin;
        if (leftFund < 0) {
            leftFund = 0
        }
        mouthdataArray[i - 1] = {
            monthName: realmonth + "月",
            yuelixi: yuelixi,
            //每月本金
            yuebenjin: everymonthyh,
            //剩余还款
            leftFund: leftFund
        }
    }
    return {
        yuegong: out1,
        totalLixi: totalLixi,
        totalPrice: totalPrice,
        yuegongdijian: yuegongdijian,
        mouthdataArray: mouthdataArray,
        totalDknum: num,
        year: year
    }

}

var rangeSlider = function() {

  // 輸入
  // po1 #loan-payment 貸款金額 100萬 ~ 2000萬
  // po2 #loan-interest 貸款年利率  0.01 ~ 5.00%
  // po3 #loan-years 貸款年限 5 ~ 40年

  // 計算出
  // out1 #total-monthy 每月還款  po1 * (1 + po2) / 12 / po3
  // out2 #total-interest 總利息  po1 * po2 
  // out3 #total-payment  總貸款(含利息) po1 * (1 + po2)

  //定義
  var po1, po2, po3, out1, out2, out3;

  //拉桿子計算數字  
  var range = $('.range-slider__range');
  var output = $('.range-slider__output');

  //計算數字
  var calcValue = function(){
    po1 = +document.getElementById('loan-payment').textContent
    po2 = +document.getElementById('loan-interest').textContent;
    po3 = +document.getElementById('loan-years').textContent;
    

    
    // type, 百萬貸款, 幾年, 利率
    benjin (0, po1, po3, po2)
    
    clacOut1 = Math.round(po1 * (1 + po2/100) / (12 * po3)* 100) / 100;
    clacOut2 = Math.round(po1 * (po2/100) * 100) / 100;
    clacOut3 = Math.round(po1 * (1 + po2/100) * 100 ) / 100;  
  
    // out1.innerHTML= clacOut1;
    // out2.innerHTML= clacOut2;
    // out3.innerHTML= clacOut3;
  }
  


  range.on('input', function(){
    inputValue = Number(this.value);
    if ($(this).prev().children().eq(0).hasClass("is-2-decimal")){
      var keep2Decimal = inputValue.toFixed(2);
    }
    console.log(inputValue)
    $(this).prev().children().eq(0).html(inputValue);
    calcValue(); //計算數字
  });

  //讀取一開始的input預設value並放到html顯示
  output.each(function(){
    var value = +$(this).parent().next().val();
    console.log(value);
    $(this).html(value);  
    calcValue(); //計算數字
  });
}

window.onload = function() {
  console.log("loadings finished");
  rangeSlider();
  console.log("rangeSlider finished");
}