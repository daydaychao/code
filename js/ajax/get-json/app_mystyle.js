var xhr = new XMLHttpRequest();

xhr.onload = function() {
  // if (xhr.status === 200) {
  let responseObject = JSON.parse(xhr.responseText);
  
  //資料幾筆
  let jsonSize = responseObject.events.length;
  console.log('有' + jsonSize + '筆資料-jsonSize');
  
  //資料餘數
  let y = jsonSize % 4;
  
  //資料成3的倍數
  let runtimes = (4 - y) + jsonSize;
  console.log('跑' + runtimes + '次-runtimes');
  
  let newContent = "";
  
  // }

  for (let i = 0; i < runtimes; i++) {
    console.log( i + '迴圈')
    if (i % 4 === 0) {
      // console.log ('打印columns')
      newContent += `<div class="columns">`;
    }
    
    //當有資料時餵資料
    if( responseObject.events[i] != null) {
      console.log('有資料')
      newContent += `<div class="card column">`;

      newContent += `<div class="card-image image">`;
      newContent += `<img class="" src="${responseObject.events[i].map}" `;
      newContent += `alt="${responseObject.events[i].location}" >`;
      newContent += `</div>`;
      
      newContent += `<div class="card-content">`;
      newContent += `<p><strong>${responseObject.events[i].location}</strong></p>`;
      newContent += `<p class="has-text-grey">${responseObject.events[i].date}</p>`;
      newContent += `</div>`;

      newContent += `</div>`;
    }
    //當無資料時長空的div
    if( responseObject.events[i] == null) {
      newContent += `<div class="card column">`;  
      newContent += `<div class="card-content">`;
      newContent += `</div>`;
      newContent += `</div>`;
    }  
    
    if (i % 4 === y) {
      newContent += `</div>`;
      console.log ('關閉columns')
    } 

  }

  document.getElementById("content").innerHTML = newContent;
};

xhr.open("GET", "data/data.json", true);
xhr.send(null);
