//Time: 929ms Passed: 27 Failed: 0
function moreZeros(s){
  let arrAscii = s.split('').map(s => s.charCodeAt()).map(s => s.toString(2));
  //charCodeAt轉成ascii,toString(2)轉成二進位

  let arrFilter = arrAscii.filter(s => s.split('0').length > s.split('1').length).map(s => parseInt(s,2));
  // split('某').length可以用字數多少>篩選 parseInt(s,2)由二進位轉成十進位

  let noDuplicates = new Set(arrFilter)
  // new Set移除重複

  return String.fromCharCode(...noDuplicates).split('');
  // fromCharCode ascii轉成字母 ...展開陣列中的值
}


moreZeros("abcde"); //?
moreZeros('Great job!')//?
moreZeros('DIGEST')//?
moreZeros('abcdeabcde')//?
