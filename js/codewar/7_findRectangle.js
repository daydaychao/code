// Find the area of a rectangle when provided with one diagonal and one side of the rectangle.
// If the input diagonal is less than or equal to the length of the side, return "Not a rectangle".
// If the resultant area has decimals round it to two places.
// This kata is meant for beginners. Rank and upvote to bring it out of beta!

//Time: 926ms Passed: 100 Failed: 0  2020/04/01
// function area(d, l) {
//   let w = d * d - l * l;
//   let size = l * Math.sqrt(w);
//   if( isNaN(size) | d == l) { return 'Not a rectangle' } 
//   return size % 1 == 0 ? size : Number(size.toFixed(2));
// }

function area(d, l) {
  let width = d * d - l * l;
  let size = l * Math.sqrt(width);
  return d <= l ? 'Not a rectangle': Number(size.toFixed(2));
}

area(5, 4); //?
area(10, 10); //?
area(13, 5); //?
area(12, 5); //?
area(62, 67); //?
area(71, 69); //?
