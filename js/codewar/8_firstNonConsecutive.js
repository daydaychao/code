//Time: 963ms Passed: 14 Failed: 0
// function firstNonConsecutive (arr) {
//   for(let i=0; i<arr.length-1; i++) {
//     if(arr[i+1] !== arr[i]+1){
//       return arr[i+1]
//     }
//   }
//   return null
// }

//Time: 970ms Passed: 1 Failed: 0
// function firstNonConsecutive(arr) {
//   for(let i=0; i<=arr.length;i++) {
//     if( (arr[i]+1 - arr[i+1]) !== 0 ) {
//       if (arr[i+1] == undefined) {
//         return null
//       }
//       return arr[i+1]
//     }
//   }
// }

//Time: 926ms Passed: 14 Failed: 0
function firstNonConsecutive(arr) {
  let result = arr.find(function(x, i) {
    return x !== i + arr[0]
  });
  return Number.isInteger(result) ? result : null
}

firstNonConsecutive([1, 2, 3, 4, 6, 7, 8]); //?
firstNonConsecutive([4, 5, 6, 7, 9, 10, 12, 13]); //?
firstNonConsecutive([0, 2, 3, 4, 6, 7, 8, 9]); //?
firstNonConsecutive([-5, -4, -3, -2, -1, 0, 1, 2, 3, 4]); //?
firstNonConsecutive([10, 11, 12, 13, 14, 15, 16, 17, 18]); //?
