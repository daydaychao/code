function divisors(integer) {
  let ans = [];

  for (i = 2; i <= integer; i++) {
    if (integer % i == 0) {
      ans.push(i);
    }
    if (integer == i) {
      ans.pop();
      if (ans == '') {
        return i + ' is prime';
      }
    }
  }
  return ans; //?
}

divisors(12); //?
divisors(25); //?
divisors(13); //?
