"use strict";

// Time: 922ms Passed: 117 Failed: 0
// function getGrade(s1, s2, s3) {
//   let score = [s1, s2, s3];
//   score = score.reduce((x, y) => x + y) / 3; //?

//   if (score >= 90 && score <= 100) {
//     return "A"; //?
//   } else if (score >= 80 && score <= 90) {
//     return "B"; //?
//   } else if (score >= 70 && score <= 80) {
//     return "C"; //?
//   } else if (score >= 60 && score <= 70) {
//     return "D"; //?
//   } else if (score >= 0 && score <= 60) {
//     return "F"; //?
//   }
// }

//Time: 970ms Passed: 117 Failed: 0
function getGrade(s1, s2, s3) {
  var avg = (s1 + s2 + s3) / 3;
  return avg >= 90 ? "A" : avg >= 80 ? "B" : avg >= 70 ? "C" : avg >= 60 ? "D" : "F";
}

getGrade(95, 90, 93); //?
getGrade(100, 85, 96); //?
getGrade(92, 93, 94); //?
getGrade(84, 79, 85); //?
getGrade(60, 82, 76); //?
getGrade(58, 62, 70); //?
getGrade(48, 55, 52); //?