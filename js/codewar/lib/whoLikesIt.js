'use strict';

/*
describe('example tests', function() {
  it('should return correct text', function() {
    Test.assertEquals(likes([]), 'no one likes this');
    Test.assertEquals(likes(['Peter']), 'Peter likes this');
    Test.assertEquals(likes(['Jacob', 'Alex']), 'Jacob and Alex like this');
    Test.assertEquals(likes(['Max', 'John', 'Mark']), 'Max, John and Mark like this');
    Test.assertEquals(likes(['Alex', 'Jacob', 'Mark', 'Max']), 'Alex, Jacob and 2 others like this');
  });
});
*/

//For迴圈解法
function likes(names) {
  for (var i = 0; i <= names.length; i++) {

    //nobody
    if (names.length == 0) {
      return names[i] = 'no one likes this'; //?
    }

    //1 person
    if (names.length == 1) {
      return names[i] = names[i] + ' likes this'; //?
    }

    //2 people
    if (names.length == 2) {
      return names[i] = names[i] + ' and ' + names[++i] + ' like this'; //?
    }

    //3 people
    if (names.length == 3) {
      return names[i] = names[i] + ', ' + names[++i] + ' and ' + names[++i] + ' like this'; //?
    }

    //>4 people
    if (names.length >= 4) {
      return names[i] = names[i] + ', ' + names[++i] + ' and ' + (names.length - 2) + ' others like this'; //?
    }
  }
}

// RESULT
likes([]); //?
likes(['Peter']); //?
likes(['Jacob', 'Alex']); //?
likes(['Max', 'John', 'Mark']); //?
likes(['Alex', 'Jacob', 'Mark', 'Max']); //?