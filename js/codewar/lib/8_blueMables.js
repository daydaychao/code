"use strict";

//Time: 938ms Passed: 103 =================================
function guessBlue(blueStart, redStart, bluePulled, redPulled) {
  var all = blueStart + redStart - bluePulled - redPulled;
  var lastBlue = blueStart - bluePulled;
  return lastBlue / all;
}

guessBlue(5, 5, 2, 3); //?
guessBlue(5, 7, 4, 3); //?
guessBlue(12, 18, 4, 6); //?