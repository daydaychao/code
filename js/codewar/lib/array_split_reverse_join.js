'use strict';

/*

Write a function that takes in a string of one or more words, and returns the same string, but with all five or more letter words reversed (Just like the name of this Kata). Strings passed in will consist of only letters and spaces. Spaces will be included only when more than one word is present.

Examples: spinWords( "Hey fellow warriors" ) => returns "Hey wollef sroirraw" spinWords( "This is a test") => returns "This is a test" spinWords( "This is another test" )=> returns "This is rehtona test"

Test.assertEquals(spinWords("Welcome"), "emocleW");
Test.assertEquals(spinWords("Hey fellow warriors"), "Hey wollef sroirraw");
Test.assertEquals(spinWords("This is a test"), "This is a test");
Test.assertEquals(spinWords("This is another test"), "This is rehtona test");
Test.assertEquals(spinWords("You are almost to the last test"), "You are tsomla to the last test");
Test.assertEquals(spinWords("Just kidding there is still one more"), "Just gniddik ereht is llits one more");
Test.assertEquals(spinWords("Seriously this is the last one"), "ylsuoireS this is the last one");
*/

// 用for迴圈 將原本字串改為陣列
function spinWords(text) {
  var newArray = [];
  newArray = text.split(' '); //?

  for (var i = 0; i < newArray.length; i++) {
    if (newArray[i].length >= 5) {
      var str = newArray[i].split('').reverse().join(''); //?
      newArray[i] = str;
    }
  }

  return newArray.join(' '); //?
}

//用map方式 new一個新陣列
// function spinWords(text) {
//   var array = []; //?
//   array = text.split(" "); //?
//   var newArray = array.map(function(element) {
//     // 數值大於五的數值視為五
//     if (element.length >= 5)
//     element = element.split('').reverse().join('')//?
//     return element //?
//   });
//   return newArray
// }

spinWords("Welcome to my new house"); //?
// console.log(spinWords("Welcome to my new house"))