"use strict";

//使用filter代入function後回傳是ture的數字

//Time: 842ms ===================================
function noOdds(values) {
  return values.filter(function (x) {
    return x % 2 == 0; //?
  });
}

// Time: 870ms ===================================
function noOdds(values) {
  var isEven = values.filter(function (item) {
    return item % 2 == 0;
  });
  return isEven;
}

//Time: 951ms ===================================
var noOdds = function noOdds(values) {
  return values.filter(function (x) {
    return x % 2 == 0;
  });
};

//Time: 955ms ===================================
function noOdds(values) {
  function isEven(number) {
    return number % 2 == 0;
  }
  return values.filter(isEven); //?
}

noOdds([0, 11, 22, 33, 44, 77, 99]); //?