"use strict";

// 將字串按順序交叉連接

// Time: 885ms Passed: 105 Failed: 0 ==============================================
function tripleTrouble(one, two, three) {
  var ans = "";
  var x = one.length;
  for (var i = 0; i < x; i++) {
    ans += one.charAt(i) + two.charAt(i) + three.charAt(i); //?
  }
  return ans;
}

//Time: 923ms ==============================================
// function tripleTrouble(one, two, three) {
//   let ans = "";
//   let x = one.length;
//   for (let i = 0; i < x ; i++) {
//     ans = ans.concat(one[i], two[i], three[i]); //?
//   }
//   return ans;
// }

//Time: 975ms ==============================================
// function tripleTrouble(one, two, three) {
//   let ans = "";
//   for (let i = 0; i < one.length; i++) {
//     ans = ans.concat(one[i], two[i], three[i]); //?
//   }
//   return ans;
// }

tripleTrouble("this", "test", "lock"); //?
tripleTrouble("aa", "bb", "cc"); //?
tripleTrouble("Bm", "aa", "tn"); //?
tripleTrouble("LLh", "euo", "xtr"); //?