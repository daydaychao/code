// 英雄的子彈夠差死龍嗎? 一隻龍需要兩顆子彈才會陣亡
// 夠 true 不夠 false

//Time: 881ms Passed: 106 Failed: 0 ====================================
// function hero(bullets, dragons){
//   return bullets/2 >= dragons
// }

//ES6 Time: 959ms Passed: 106 Failed: 0
const hero = (bullets, dragons) => bullets/2 >= dragons


hero(10, 5)//?
hero(7, 4)//?
hero(4, 5)//?
hero(100, 40)//?
hero(1500, 751)//?
hero(0, 1)//?