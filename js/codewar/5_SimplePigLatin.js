// Move the first letter of each word to the end of it, then add "ay" to the end of the word. 
// Leave punctuation marks untouched.

// pigIt('Pig latin is cool'); // igPay atinlay siay oolcay
// pigIt('Hello world !');     // elloHay orldway 

// Time: 939ms Passed: 21 Failed: 0
// function pigIt(str){
//   let newArray = [];
//   str.split(' ').forEach(function(item){
//     let newItem = item.slice(1).concat( item[0] );
//     //+ay and push
//     if( !item.match(/^[.,:!?]/) ) {
//       newItem = newItem.concat('ay');//?
//       newArray.push(newItem)
//     }
//     //do not +ay and push
//     else {
//       newArray.push(newItem)
//     }
//   })
//   return newArray.join(' ')//?
// }

//use includes
//Time: 849ms Passed: 21 Failed: 0
// function pigIt(str){
//   let marks = '!.?,';
//   return str.split(' ').map(item => {
//       return marks.includes(item)? item : item.slice(1) + item.slice(0,1) + 'ay';
//     }
//   ).join(' ');
// }

// function pigIt(str) {
//   return str.replace(/\w+/g,function(word){
//     return word.slice(1) + word[0] + 'ay'
//   })
// }

//Time: 915ms Passed: 21 Failed: 0
function pigIt(str) {
  return str.replace(/\w+/g,(word) => word.slice(1) + word[0] + 'ay')
}

pigIt('Pig latin is cool')//?
pigIt('good day !'); //?