// 西瓜可以偶數分給兩個人嗎?

// function divide(weight) {
//   if (weight % 2 == 0) {
//     return true;
//   } else {
//     return false;
//   }
// }

function divide(weight){
  return weight >= 4 && (weight % 2 == 0); //?
}

divide(4); //?
divide(2); //?
divide(5); //?
divide(88);  //?
divide(100); //?
divide(67); //?
divide(10); //?
divide(99); //?
divide(32); //?