// Make connection
var socket = io.connect('http://localhost:4000');

// Query Dom

var $userForm = $('#user-form');
var $login = $('#login');
var $username = $('#username');

var $users = $('#users');
var $message = $('#message');
var $send = $('#send');
var $output = $('#output');
var $feedback = $('#feedback');

var $chatWindow = $('#chat-window');

// Emit events - new user
$username.on('keypress', function (e) {
  if (event.keyCode == 13) {
    e.preventDefault();
    console.log('entered');
    emitUsername();
  }
});
$login.on('click', function (e) {
  e.preventDefault();
  emitUsername();
});
function emitUsername() {
  socket.emit('new user', $username.val(), function (data) {
    if (data) {
      $userForm.hide();
      $chatWindow.show();
    }
  });
  $username.val('');
}

// Emit events - typing 當有人在打字時的推廣
// $message.on('keypress', function () {
//   socket.emit('typing', $handle.val());
// });

// Emit events - chat
$send.on('click', function () {
  // 1名稱 2資料
  socket.emit('chat', {
    msg: $message.val(),
  });
});
// Listen for events - chat
function emitChat() {
  socket.on('chat', function (data) {
    feedback.innerHTML = '';
    output.innerHTML += `<p class="chat-user"><i class="user icon"></i><strong>${data.user}</strong></p><div class="chat-bobo">${data.msg}</div></p>`;
  });
}

// Listen for events - typing
socket.on('typing', function (data) {
  feedback.innerHTML += `<p class="chat-waiting"><i class="pencil alternate icon"></i>${data} is typing a message...</p>`;
});

// get users
socket.on('get users', function (data) {
  console.log(data);
  var html = '';
  for (i = 0; i < data.length; i++) {
    html += '<div class="get-users">' + data[i] + '</div>';
  }
  $users.html(html);
});
