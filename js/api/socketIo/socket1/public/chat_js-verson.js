// Make connection
var socket = io.connect('http://localhost:4000');

// Query Dom
var message = document.getElementById('message');
var handle = document.getElementById('handle');
var btn = document.getElementById('send');
var output = document.getElementById('output');
var feedback = document.getElementById('feedback');

// Emit events - chat
btn.addEventListener('click', function () {
  // 1名稱 2資料
  socket.emit('chat', {
    message: message.value,
    handle: handle.value,
  });
});
// Emit events - typing
message.addEventListener('keypress', function () {
  socket.emit('typing', handle.value);
});

// Listen for events - chat
socket.on('chat', function (data) {
  feedback.innerHTML = '';
  output.innerHTML += `<p class="chat-user"><i class="user icon"></i><strong>${data.handle}</strong></p><div class="chat-bobo">${data.message}</div></p>`;
});

// Listen for events - typing
socket.on('typing', function (data) {
  feedback.innerHTML += `<p class="chat-waiting"><i class="pencil alternate icon"></i>${data} is typing a message...</p>`;
});
