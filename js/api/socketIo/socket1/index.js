// Server Setup
var express = require('express');
var app = express();
var server = app.listen(process.env.PORT || 4000, function () {
  console.log('監聽 4000 的需求request');
});
// Static files css 等檔案放此
app.use(express.static('public'));

// Socket Setup
var socket = require('socket.io');
var io = socket(server);
var connections = [];
var users = [];

io.on('connection', function (socket) {
  connections.push(socket);
  console.log(
    `有人連線:${socket.id}, %s sockets connected`,
    connections.length
  );

  // Disconnect 斷線
  socket.on('disconnect', function (data) {
    users.splice(users.indexOf(socket.username), 1);
    updateUsernames();
    connections.splice(connections.indexOf(socket), 1);
    console.log(
      `有人離線:${socket.id}, %s sockets connected`,
      connections.length
    );
  });

  // chat 聊天內容
  socket.on('chat', function (data) {
    io.sockets.emit('chat', { msg: data.msg, user: socket.username });
  });

  // typing
  socket.on('typing', function (data) {
    socket.broadcast.emit('typing', data);
  });

  // new User
  socket.on('new user', function (data, callback) {
    callback(true);
    socket.username = data;
    users.push(socket.username);
    updateUsernames();
  });

  function updateUsernames() {
    console.log(users);
    io.sockets.emit('get users', users);
  }
});
