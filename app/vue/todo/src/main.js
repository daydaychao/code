import Vue from "vue";
import App from "./App.vue";

// ajax axios + vue-axios
import axios from 'axios'
import VueAxios  from 'vue-axios'
Vue.use(VueAxios, axios)

// 讀取字體
import "./assets/fonts.css";

// BS4
import "bootstrap/scss/bootstrap.scss";

// lightbox
import CoolLightBox from "vue-cool-lightbox";
import "vue-cool-lightbox/dist/vue-cool-lightbox.min.css";
export default {
  components: {
    CoolLightBox,
  },
};
Vue.use(CoolLightBox);

Vue.config.productionTip = true;

new Vue({
  render: (h) => h(App),
}).$mount("#app");
