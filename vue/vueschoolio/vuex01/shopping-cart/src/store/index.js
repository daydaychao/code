import Vue from 'vue'
import Vuex from 'vuex'
import shop from '@/api/shop'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    products: [],
    cart: []
  },
  modules: {},
  getters: {
    // 適合過濾與計算

    availableProducts(state, getters) {
      return state.products.filter(product => product.inventory > 0)
    },

    productsCount() {
      // ...
    }
  },
  actions: {
    // = methods
    // context.commit提交
    // context.state和context.getters來獲取state和getters
    // action can be complex but never update the status

    // addToCart({ commit }, product) {
    //   if (product.inventory > 0) {
    //     commit('pushProductToCart', product)
    //   } else {
    //     // show out of stock messages
    //   }
    // },

    addProductToCart(context, product) {
      if (product.inventory > 0) {
        //find cartItem
        const cartItem = context.state.cart.find(item => item.id === product.id)
        console.log(cartItem)
        if (!cartItem) {
          context.commit('pushProductToCart', product.id)
        } else {
          //增加數量
          context.commit('incrementItemQuantity', cartItem)
        }
        //減少庫存
        context.commit('decrementProductInventory', product)
      }
    },
    fetchProducts({ commit }) {
      return new Promise((resolve, reject) => {
        shop.getProducts(products => {
          // store.state.products = products; 不要使用這種方式
          commit('setProducts', products)
          resolve()
        })
      })
    }
  },
  mutations: {
    // 更改store狀態(state)是提交mutation 例如用store.commit('setProducts' , xx) xx是帶入值

    setProducts(state, products) {
      // update products and setting the products array
      state.products = products
    },

    pushProductToCart(state, productId) {
      state.cart.push({
        id: productId,
        quantity: 1
      })
    },

    incrementItemQuantity(state, cartItem) {
      cartItem.quantity++
    },

    decrementProductInventory(state, cartItem) {
      cartItem.inventory--
    }
  }
})
