var app = new Vue({
  el: "#app",
  data: {
    todos: [],
    inputTitle: "",
    visibility: "all",
    cacheTodo: {},
    cacheTitle: ""
  },
  methods: {
    addToto: function() {
      var valueTitle = this.inputTitle.trim();
      var valueStamp = Math.floor(Date.now());
      if (valueTitle) {
        this.todos.push({
          id: valueStamp,
          title: valueTitle,
          complete: false
        });
      }
      this.inputTitle = "";
    },

    editTodo: function(todo) {
      this.cacheTodo = todo;
      this.cacheTitle = todo.title;
    },

    doneEditTodo: function(todo) {
      todo.title = this.cacheTitle;
      this.cacheTitle = "";
      this.cacheTodo = {};
    },

    exitTodo: function(todo) {
      this.cacheTodo = {};
    },

    removeTodo: function(todo) {
      var newIndex = "";
      this.todos.forEach(function(todosItem, key) {
        if (todo.id == todosItem.id) {
          newIndex = key;
        }
      });
      this.todos.splice(newIndex, 1);
    },

    clearTodos: function(todo) {
      this.todos = [];
    }
  },
  computed: {
    filterTodos: function() {
      if (this.visibility == "all") {
        var newTodos = this.todos;
        return newTodos;
      } else if (this.visibility == "going") {
        var newTodos = [];
        this.todos.forEach(function(item) {
          if (!item.complete) {
            newTodos.push(item);
          }
        });
        return newTodos;
      } else if (this.visibility == "done") {
        var newTodos = [];
        this.todos.forEach(function(item) {
          if (item.complete) {
            newTodos.push(item);
          }
        });
        return newTodos;
      }
    },
    countNumber: function() {
      var count = 0;
      this.todos.forEach(function(item) {
        if (item.complete == false) {
          count += 1;
        }
      });
      return count;
    }
  }
});
