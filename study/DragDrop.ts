export class DragDrop {
  private _parent_element: any;
  private _drapDrop_element: any;
  private _isChromeFlag: boolean; //it's a judgments flag because mouse-down event and mouse-move event will be triggered at the same time on chrome browser.
  private _attachMode: number;  // 1: attach top/bottom side 2: attach left/right side 3. both 4. none
  private _status: string;  // 1: start 2: move 3: stop
  private currentX: number;
  private currentY: number;

  private touchStartCallback: ()=>{};
  private touchMoveCallback: ()=>{};
  private touchEndCallback: ()=>{};
  private clickCallback: ()=>{};

  get parent_element(): any {
    return this._parent_element;
  }

  set parent_element(value: any) {
    this._parent_element = value;
  }

  get drapDrop_element(): any {
    return this._drapDrop_element;
  }

  set drapDrop_element(value: any) {
    this._drapDrop_element = value;
  }

  get isChromeFlag(): boolean {
    return this._isChromeFlag;
  }

  set isChromeFlag(value: boolean) {
    this._isChromeFlag = value;
  }

  get attachMode(): number {
    return this._attachMode;
  }

  set attachMode(value: number) {
    this._attachMode = value;
  }

  get status(): string {
    return this._status;
  }

  set status(value: string) {
    this._status = value;
  }

  constructor(dragDrop: string, parent?: string) {
    if (!parent) {
      parent = "body";
    }
    this.parent_element = document.querySelector(parent);
    this.drapDrop_element = document.querySelector(dragDrop);

    this.drapDrop_element.style.position = "absolute";
    this.bindEvent();
  }

  public destroy() {
    window.removeEventListener("touchstart", this.dragStart_handler);
    window.removeEventListener("mousedown", this.dragStart_handler);

    window.removeEventListener("touchmove", this.dragStart_handler);
    window.removeEventListener("mousemove", this.dragStart_handler);

    window.removeEventListener("touchend", this.dragStart_handler);
    window.removeEventListener("mouseup", this.dragStart_handler);
  }

  private bindEvent() {
    window.addEventListener("touchstart", (event) => {
      if(event.target === this.drapDrop_element){
        this.dragStart_handler(event);
      }
    });
    window.addEventListener("mousedown", (event) => {
      if(event.target === this.drapDrop_element) {
        this.dragStart_handler(event);
      }
    });

    window.addEventListener("touchmove", (event) => {
      if(event.target === this.drapDrop_element) {
        this.dragMove_handler(event);
      }
    });
    window.addEventListener("mousemove", (event) => {
      if(event.target === this.drapDrop_element) {
        this.dragMove_handler(event);
      }
    });

    window.addEventListener("touchend", (event) => {
      if(event.target === this.drapDrop_element) {
        this.dragEnd_handler(event);
      }
    });
    window.addEventListener("mouseup", (event) => {
      if(event.target === this.drapDrop_element) {
        this.dragEnd_handler(event);
      }
    });
  }

  private dragStart_handler(event) {
    this.status = "start";
    if(this.touchStartCallback){
      this.touchStartCallback();
    }
  }

  private dragMove_handler(event) {
    if (this.status === "start") {
      if (this.isChromeFlag) {
        this.status = "move";
      } else {
        this.isChromeFlag = true;
      }
    } else if (this.status === "move") {
      let axis = event.pageX ? event : event.touches[0];
      let x = axis.pageX - this.drapDrop_element.clientWidth/2 - window.pageXOffset;
      let y = axis.pageY - this.drapDrop_element.clientHeight/2 - window.pageYOffset;

      if(x >= 0 && x <= this.parent_element.clientWidth - this.drapDrop_element.clientWidth){
        this.drapDrop_element.style.left = x + "px";
        this.currentX = x;
      }
      if(y >= 0 && y <= this.parent_element.clientHeight - this.drapDrop_element.clientHeight){
        this.drapDrop_element.style.top = y + "px";
        this.currentY = y;
      }
    }
  }

  private dragEnd_handler(event) {
    if (this.status === "move") {
      if(this.attachMode!=4){
        this.paste();
      }
      if (this.touchMoveCallback) {
        this.touchMoveCallback();
      }
    } else if (this.status === "start") {
      if (this.touchEndCallback) {
        this.touchEndCallback();
      }
    }

    if (this.clickCallback) {
      this.clickCallback();
    }

    this.status = "stop";
    this.isChromeFlag = false;
  }

  private paste() {
    let x: number = 0;
    let y: number = 0;

    if (this.currentX > this.parent_element.clientWidth / 2 - this.drapDrop_element.clientWidth / 2) {    //right
      x = this.parent_element.clientWidth + this.parent_element.offsetLeft - this.drapDrop_element.clientWidth;
    } else {        //left
      x = 0 + this.parent_element.offsetLeft;
    }
    if (this.currentY > this.parent_element.clientHeight / 2 - this.drapDrop_element.clientHeight / 2) {    //bottom
      y = this.parent_element.clientHeight + this.parent_element.offsetTop - this.drapDrop_element.clientHeight;
    } else {        //top
      y = 0 + this.parent_element.offsetTop;
    }

    let xDistance: number = x - this.currentX;
    let yDistance: number = y - this.currentY;

    switch (this.attachMode) {
      case 1: //top & bottom
        TweenMax.to(this.drapDrop_element, .1, {
          top: y + "px"
          , onComplete: () => {
            this.drapDrop_element.style.top = y + "px";
          }
        });
        break;
      case 2: //left & right
        TweenMax.to(this.drapDrop_element, .1, {
          left: x + "px"
          , onComplete: () => {
            this.drapDrop_element.style.left = x + "px";
          }
        });
        break;
      case 3: //both
      default:
        if (Math.abs(xDistance) > Math.abs(yDistance)) {  //top & bottom
          TweenMax.to(this.drapDrop_element, .1, {
            top: y + "px"
            , onComplete: () => {
              this.drapDrop_element.style.top = y + "px";
            }
          });
        } else { //left & right
          TweenMax.to(this.drapDrop_element, .1, {
            left: x + "px"
            , onComplete: () => {
              this.drapDrop_element.style.left = x + "px";
            }
          });
        }
        break;
    }
  }
}
